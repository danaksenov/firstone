package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText text;
    Button buttonView;
    Button buttonRemove;
    TextView show;
    //String myString1;
    //String myString2;
    int i = 0, m = 1;
    ArrayList<Editable> currentText = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);
        buttonRemove = findViewById(R.id.button_remove);
        buttonView = findViewById(R.id.button_view);

        buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentText.add((Editable) text.getText());
                text.setText("");
                m = 1;
            }
        });

        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = currentText.size() - m;
                text.setText((String.format("%s", currentText.get(i))));
                m++;
            }
        });
    }
}

//
//
//    }
//
//    public void viewText(View v) {
//        EditText text = findViewById(R.id.text);
//        TextView show= findViewById(R.id.view);
//        show.setText("Добро пожаловать, " + text.getText());
//    }
//
//
//    public void removeText(View view) {
//        EditText text = (EditText) findViewById(R.id.text);
//        TextView show= (TextView) findViewById(R.id.view);
//        show.setText(" ");
//    }


//    @Override
//    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
//        super.onSaveInstanceState(outState, outPersistentState);
//        outState.putString("str",myString1);
//        outState.putString("str2", myString2);
//    }
//
//    @Override
//    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
//        super.onRestoreInstanceState(savedInstanceState, persistentState);
//        myString1 = savedInstanceState.getString("str");
//        myString2 = savedInstanceState.getString("str2");
//    }



